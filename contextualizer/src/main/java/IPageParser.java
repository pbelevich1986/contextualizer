import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by avenkatraman on 1/9/15.
 */
public interface IPageParser {

    Map<String, Integer> parse(String str, Map<String, Set<String>> categories) throws IOException;
}
