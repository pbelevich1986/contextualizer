import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by avenkatraman on 1/9/15.
 */
public class LineParser implements ILineParser {

    private Counter counter;

    public Counter getCounter() {
        return counter;
    }

    public void setCounter(Counter counter) {
        this.counter = counter;
    }

    @Override
    public Map<String, Integer> parse(String str, Map<String, Set<String>> categories) throws IOException {
        Map<String, Integer> weights = new HashMap<String, Integer>();
        for (String categoryName : categories.keySet()) {
            weights.put(categoryName, getCategoryWeight(str, categories.get(categoryName)));
        }
        return weights;
    }

    private int getCategoryWeight(String line, Set<String> words) {
        int categoryWeight = 0;
        for (String word : words) {
            categoryWeight = categoryWeight + counter.count(line, word);
        }
        return categoryWeight;
    }


}
